Este aplicativo foi desenvolvido a partir de um curso realizado na plataforma de ensino online UDEMY.

Para testar a aplicação você irá seguir os seguintes procedimentos:

1 - Instalação do NODE.JS : https://nodejs.org/en/download/

2 - Instalação NPM: https://www.npmjs.com/get-npm

3 - Instalação react-native:

    3.1- Abra o prompt comand do seu computador e digite: npm install -g create-react-native-app

4-  Este app está preparado para a plataforma android, então você irá precisar ter o android studio instalado:

    4.1- Seguir o procedimento: https://facebook.github.io/react-native/docs/getting-started.html > Building Projects with native code

5- Com o Android Studio aberto, abrir este código fonte. Deixe o gradle terminar seus processos.

6- Abra o dispostivo virtual que voce criou no passo 4.

7- Via prompt comando, acesse a pasta onde se encontra este código fonte, dentro dele, digite: react-native run-android

Contato: michelvictor16@gmail.com