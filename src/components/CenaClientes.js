/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Image,
  StyleSheet,
  Text
} from 'react-native';

import BarraNavegacao from './BarraNavegacao'

const detalheCliente = require('../img/detalhe_cliente.png');
const cliente1 = require('../img/cliente1.png');
const cliente2 = require('../img/cliente2.png');


export default class CenaClientes extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#FFF'}}>
        <StatusBar 
          backgroundColor ='#B9C941'
        />
        <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#B9C941'/>
        
        <View style={styles.cabecalho}>
          <Image source={detalheCliente}/>
          <Text style={styles.txtTitulo} >Nossos Clientes</Text>
        </View>

        <View style={styles.clienteDetalhe}>
          <Image source={cliente1}/>
          <Text style={styles.txtDetalheCliente}>Lorem Ipsum dolorem</Text>
        </View>
        
        <View style={styles.clienteDetalhe}>
          <Image source={cliente2}/>
          <Text style={styles.txtDetalheCliente} >Lorem Ipsum dolorem</Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  cabecalho:{
    flexDirection: 'row',
    marginTop: 20
  },
  txtTitulo:{
    fontSize: 30,
    color: '#B9C941',
    marginLeft: 10,
    marginTop: 25
  },
  clienteDetalhe:{
    padding: 20,
    marginTop: 10
  },
  txtDetalheCliente:{
    fontSize: 18,
    marginLeft: 20
  }
});