/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

export default class BarraNavegacao extends Component {
  render() {
    if(this.props.voltar){
      return (
        <View style={[styles.barraTitulo, { backgroundColor: this.props.corDeFundo }]}>
            <TouchableHighlight 
            underlayColor={this.props.corDeFundo}
            activeOpacity={0.3} 
              onPress={()=>{
                this.props.navigator.pop();
              }}
            >
              <Image source={require('../img/btn_voltar.png')}/>
            </TouchableHighlight>
            <Text style={styles.titulo}>ATM Consultoria</Text>
        </View>
      );
    }

    return (
      <View style={styles.barraTitulo}>
          <Text style={styles.titulo}>ATM Consultoria</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    barraTitulo:{
        backgroundColor: '#CCC',
        padding: 10,
        height: 60,
        flexDirection: 'row'
    },
    titulo:{
        flex: 1,
        fontSize: 18,
        textAlign: 'center'
    }
})
